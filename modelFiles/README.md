## Introduction

This folder holds some examples for modelling growth and cell division using either a cell-based
or a vertex-based approach. The software for running simulations can be found at

<p>https://gitlab.com/slcu/teamhj/tissue</p>
<p>and</p>
<p>https://gitlab.com/slcu/teamhj/organism</p>

